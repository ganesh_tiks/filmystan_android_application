package com.sysanenet.filmystan.favourite


import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sysanenet.filmystan.R
import com.sysanenet.filmystan.base.AppDatabaseHelper
import com.sysanenet.filmystan.dashboard.DashboardActivity
import com.sysanenet.filmystan.dialouges.DialougesAdapter
import com.sysanenet.filmystan.dialouges.OnDialogListener
import com.sysanenet.filmystan.model.DialogueModel
import com.sysanenet.filmystan.statusmaker.StatusMakerActivity
import kotlinx.android.synthetic.main.fragment_favourite.*


/**
 * A simple [Fragment] subclass.
 */
class FavouriteFragment : Fragment(), OnFavouriteListener, OnDialogListener {



    private var adapter: DialougesAdapter? = null
    private var rootView: View? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.fragment_favourite, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initUI()
        DialogLoader(activity, this).execute()
    }

    override fun onStart() {
        super.onStart()
        shimmerViewContainer.startShimmerAnimation()
    }


    override fun onPause() {
        super.onPause()
        shimmerViewContainer.stopShimmerAnimation()

    }


    override fun onDataLoaded(data: List<DialogueModel>) {

        shimmerViewContainer.stopShimmerAnimation()
        shimmerViewContainer.visibility = View.GONE

        if (!data.isEmpty()) {
            favouriteRecyclerView.visibility = View.VISIBLE
            adapter!!.updateData(data)
        } else {
            favouriteRecyclerView.visibility = View.GONE
            textViewNoFavourite.visibility = View.VISIBLE
        }
    }

    override fun onShareTapped(dialogueModel: DialogueModel) {
        // do it later
        openShareIntent(dialogueModel)
    }

    override fun onFavouriteTapped(dialogueModel: DialogueModel) {
        val dbHelper = AppDatabaseHelper(activity)
        dbHelper.removeDialog(dialogueModel)
        DialogLoader(activity, this).execute()
        (activity as DashboardActivity).showSnackBar("Remove dialogue from favourite")
    }

    override fun onLongPressed(dialogueModel: DialogueModel) {
        val dialogue = dialogueModel.dialog
        val movieName = dialogueModel.movie_name.replace("\\s".toRegex(), "")
        val actorName = dialogueModel.actor_name.replace("\\s".toRegex(), "")
        val shareMessage: String = "$dialogue\n Movie: #$movieName\nActor: #$actorName"
        val clipboardService = activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("text", shareMessage)
        clipboardService.primaryClip = clipData

        val parentActivity = activity as DashboardActivity
        parentActivity.showSnackBar("Dialogue copied to clip board")
    }

    override fun onWhatsAppTapped(dialogueModel: DialogueModel) {
        val statusMakerIntent = Intent(activity, StatusMakerActivity::class.java)
        statusMakerIntent.putExtra("dialogue",dialogueModel.dialog)
        startActivity(statusMakerIntent)
    }


    /**
     * Init UI
     */
    private fun initUI() {

        adapter = DialougesAdapter(activity, this)

        favouriteRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        favouriteRecyclerView.itemAnimator = DefaultItemAnimator()
        favouriteRecyclerView.adapter = adapter

    }

    /**
     * Share a dialouge using share intent
     */
    private fun openShareIntent(dialogueModel: DialogueModel) {
        val shareMessage: String = dialogueModel.dialog + "\n" + dialogueModel.movie_name + "\n" + dialogueModel.actor_name + "\n Get Application: https://play.google.com/store/apps/details?id=com.sysanenet.filmystan&hl=en"

        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
        shareIntent.type = "text/plain"
        startActivity(shareIntent)
    }


    companion object {
        fun getInstance(): FavouriteFragment = FavouriteFragment()
    }


}