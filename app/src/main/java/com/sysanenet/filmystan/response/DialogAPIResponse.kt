package com.sysanenet.filmystan.response

import android.support.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.sysanenet.filmystan.model.DialogueModel

/**
 * Created by ganeshtikone on 22/3/18.
 * class: DialogAPIResponse
 */
@Keep
class DialogAPIResponse(
        @SerializedName("status") val status: Int,
        @SerializedName("data") val data: List<DialogueModel>,
        @SerializedName("msg") val msg: String)