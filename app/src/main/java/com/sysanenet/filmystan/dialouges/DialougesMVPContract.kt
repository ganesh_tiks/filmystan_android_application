package com.sysanenet.filmystan.dialouges

import com.sysanenet.filmystan.base.BasePresenter
import com.sysanenet.filmystan.base.BaseViewOpration
import com.sysanenet.filmystan.model.DialogueModel

/**
 * Created by ganeshtikone on 22/3/18.
 * Interface: DialougesMVPContract
 */
interface DialougesMVPContract {

    /**
     * View Call Back interface for Activity
     */
    interface ViewCallBackOperations : BaseViewOpration {

        fun onDataReceived(randomDialog: List<DialogueModel>)

        fun onError(error: String)
    }

    /**
     * View Presenter Operation
     */
    interface ViewPresenterOperations : BasePresenter {

        fun loadRecenetMovies(movieId: String)
    }
}