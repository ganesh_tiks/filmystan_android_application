package com.sysanenet.filmystan.favourite

import com.sysanenet.filmystan.model.DialogueModel

/**
 * Created by ganeshtikone on 24/03/18.
 * Interface: OnFavouriteListener
 */
interface OnFavouriteListener {
    fun onDataLoaded(data:List<DialogueModel>)
}