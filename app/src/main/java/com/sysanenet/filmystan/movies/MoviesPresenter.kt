package com.sysanenet.filmystan.movies

import com.android.volley.Request
import com.google.gson.Gson
import com.sysanenet.filmystan.network.APIEndPoints
import com.sysanenet.filmystan.network.RESTExecutor
import com.sysanenet.filmystan.network.RESTInterface
import com.sysanenet.filmystan.response.RecentAPIResponse
import org.json.JSONObject
import java.lang.ref.WeakReference

/**
 * Created by ganeshtikone on 23/03/18.
 * Class: MoviesPresenter
 */
class MoviesPresenter(viewCallback:MoviesMVPContract.ViewCallBackOperations): MoviesMVPContract.ViewPresenterOperations, RESTInterface {

    private var mViewCallBack:WeakReference<MoviesMVPContract.ViewCallBackOperations>?=null
    init {
        this.mViewCallBack = WeakReference(viewCallback)
    }

    override fun onDestroy() {
        mViewCallBack = null
    }

    override fun loadMovies(page: Int) {
        val apiURL = APIEndPoints.MOVIES_DIALOG_URL + page
        val restExecutor = RESTExecutor(1005, Request.Method.GET, apiURL)
        restExecutor.setListener(this)
        restExecutor.buildJsonObjectRequest()
    }


    override fun onSuccess(requestCode: Int, data: JSONObject?, error: String?) {

        if (1005 == requestCode){
            handleMoviesResponse(data,error)
        }
    }

    /**
     * Handle Movie Response
     */
    private fun handleMoviesResponse(data: JSONObject?, error: String?) {
        if (null == error && null != mViewCallBack) {
            val gson = Gson()
            val recentMoviesResponse: RecentAPIResponse = gson.fromJson(data.toString(), RecentAPIResponse::class.java)
            mViewCallBack!!.get()!!.onDataReceived(recentMoviesResponse.data)
        } else if (null != mViewCallBack) {
            mViewCallBack!!.get()!!.onError(error!!)
        }
    }
}