package com.sysanenet.filmystan.dashboard

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem

import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.kobakei.ratethisapp.RateThisApp
import com.sysanenet.filmystan.R
import com.sysanenet.filmystan.favourite.FavouriteFragment
import com.sysanenet.filmystan.movies.MoviesFragment
import com.sysanenet.filmystan.popular.PopularFragment
import com.sysanenet.filmystan.recent.RecentFragment
import com.sysanenet.filmystan.search.SearchActivity
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {

    private var doublBackToExitPressedOnce: Boolean = false

    private var navigationListener: BottomNavigationView.OnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        when (item.itemId) {
            R.id.navigation_recent -> consume { navigationToRecent() }
            R.id.navigation_movies -> consume { navigationToMovies() }
            R.id.navigation_favorite -> consume { navigationToFavourite() }
            R.id.navigation_trending -> consume { navigationToPopular() }
            else -> super.onOptionsItemSelected(item)
        }

    }


    private var adView: AdView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setSupportActionBar(toolbar)


        bottomViewNavigation.setOnNavigationItemSelectedListener(navigationListener)
        bottomViewNavigation.selectedItemId = R.id.navigation_recent


        initialisAndLoadAd()
        initApplicationRate()

        supportActionBar?.title = getString(R.string.title_recent)
    }


    private fun initApplicationRate() {
        val config = RateThisApp.Config(3, 5)
        RateThisApp.init(config)
        RateThisApp.showRateDialogIfNeeded(this)
    }


    override fun onResume() {
        super.onResume()
        adView!!.resume()
    }

    override fun onPause() {
        super.onPause()
        adView!!.pause()
    }

    override fun onDestroy() {
        adView!!.destroy()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (doublBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        doublBackToExitPressedOnce = true
        Toast.makeText(this, "press back once again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable {
            doublBackToExitPressedOnce = false
        }, 2000)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_movie, menu)

        val searchMenuItem = menu!!.findItem(R.id.search)
        val searchView = searchMenuItem.actionView as SearchView
        searchMovie(searchView)
        return true
    }

    /**
     * Search movie
     */
    private fun searchMovie(searchView: SearchView) {
        searchView.setOnQueryTextListener(SearchMovieListener())
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return super.onOptionsItemSelected(item)
    }


    private fun initialisAndLoadAd() {
        adView = findViewById(R.id.adView)
        createAdRequest()
    }

    /**
     * Create Ad Request
     */
    private fun createAdRequest() {

        val adRequest = AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)

                .build()
        adView!!.loadAd(adRequest)

    }


    private fun navigationToPopular() {
        toolbar.title = getString(R.string.title_trending)
        loadFragment(PopularFragment.getInstance())
    }

    /**
     * Navigation to Favourite
     */
    private fun navigationToFavourite() {
        toolbar.title = getString(R.string.title_favorite)
        loadFragment(FavouriteFragment.getInstance())
    }

    /**
     * Navigation to Movies
     */
    private fun navigationToMovies() {
        toolbar.title = getString(R.string.title_movies)
        loadFragment(MoviesFragment.getInstance())
    }

    /**
     * Navigation to recent fragment
     */
    private fun navigationToRecent() {
        toolbar.title = getString(R.string.title_recent)
        loadFragment(RecentFragment.getInstance())
    }


    private fun loadFragment(fragment: Fragment) {

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit()
    }

    inline fun consume(f: () -> Unit): Boolean {
        f()
        return true
    }

    fun showSnackBar(error: String) {

        val errorSnackbar = Snackbar.make(coordinatorView, error, Snackbar.LENGTH_SHORT)

        errorSnackbar.view.setBackgroundColor(Color.parseColor("#FFC107"))

        errorSnackbar.setActionTextColor(Color.parseColor("#00796B"))

        val textView = errorSnackbar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        textView.setTextColor(Color.WHITE)

        errorSnackbar.show()
    }

    /**
     * Implemented search movie listener
     */
    inner class SearchMovieListener : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {

            Log.e("Search Query", query)
            if (query!!.length > 2) {
                val searchIntent = Intent(applicationContext, SearchActivity::class.java)
                searchIntent.putExtra("searchQuery", query)
                startActivity(searchIntent)
                return true
            }
            showSnackBar("Need more characters to search from database")
            return false
        }

        override fun onQueryTextChange(query: String?): Boolean {
            // not required
            return false
        }

    }

}
