package com.sysanenet.filmystan.spalsh

import com.android.volley.Request
import com.sysanenet.filmystan.model.MovieDialogModel
import com.sysanenet.filmystan.network.APIEndPoints
import com.sysanenet.filmystan.network.RESTExecutor
import com.sysanenet.filmystan.network.RESTInterface
import org.json.JSONObject
import java.lang.ref.WeakReference

/**
 * Created by ganeshtikone on 20/3/18.
 * Class: FilmyStanSplashPresenter
 */
class FilmyStanSplashPresenter(mViewCallBack: SplashMVPContract.ViewCallBackOperations) : SplashMVPContract.ViewPresenterOperations , RESTInterface{


    private var viewCallBack: WeakReference<SplashMVPContract.ViewCallBackOperations>? = null

    init {
        viewCallBack = WeakReference(mViewCallBack)
    }

    override fun onDestroy() {
        viewCallBack = null
    }

    override fun loadRandomDialog() {

        val restExecutor = RESTExecutor(1000,Request.Method.GET,APIEndPoints.RANDOM_DIALOG_URL)
        restExecutor.setListener(this)
        restExecutor.buildJsonObjectRequest()
    }

    override fun onSuccess(requestCode: Int, data: JSONObject?, error: String?) {

        if (1000 == requestCode){
            handleRandomDialogResponse(data,error)
        }

    }

    /**
     * Handle Random Dialog Response
     */
    private fun handleRandomDialogResponse(data: JSONObject?, error: String?) {
        if (null == error){
            viewCallBack!!.get()!!.onDataReceived(convertJsonObjectToList(data))
        }else{
            viewCallBack!!.get()!!.onError(error)
        }
    }

    /**
     * Convert JSON Object to List
     */
    private fun convertJsonObjectToList(data: JSONObject?) : List<MovieDialogModel>{
        val movies = data!!.getJSONArray("data")
        val length = movies.length()
        val movieList = mutableListOf<MovieDialogModel>()
        for (index in 0 until length){

            val movieJSONObject = movies.getJSONObject(index)
            val movie = MovieDialogModel(
                    movieJSONObject.getString("dialog_id"),
                    movieJSONObject.getString("dialog"),
                    movieJSONObject.getString("tag"),
                    movieJSONObject.getString("movie_id"),
                    movieJSONObject.getString("movie_name"),
                    movieJSONObject.getString("actor_id"),
                    movieJSONObject.getString("actor_name")
            )
            movieList.add(movie)
        }
        return movieList
    }

    companion object {
        private val TAG = FilmyStanSplashPresenter::class.java.simpleName
    }
}