package com.sysanenet.filmystan.popular

import com.android.volley.Request
import com.google.gson.Gson
import com.sysanenet.filmystan.network.APIEndPoints
import com.sysanenet.filmystan.network.RESTExecutor
import com.sysanenet.filmystan.network.RESTInterface
import com.sysanenet.filmystan.response.RecentAPIResponse
import org.json.JSONObject
import java.lang.ref.WeakReference

/**
 * Created by ganeshtikone on 22/3/18.
 * Class: PopularPresenter
 */
class PopularPresenter(viewCallBackOperations: PopularMVPContract.ViewCallBackOperations) : PopularMVPContract.ViewPresenterOperations, RESTInterface {

    private var mViewCallBack: WeakReference<PopularMVPContract.ViewCallBackOperations>? = null

    init {
        mViewCallBack = WeakReference(viewCallBackOperations)
    }

    override fun onDestroy() {
        mViewCallBack = null
    }

    override fun loadPopularMovies() {
        val restExecutor = RESTExecutor(1004, Request.Method.GET, APIEndPoints.POPULAR_DETAIL_DIALOG_URL)
        restExecutor.setListener(this)
        restExecutor.buildJsonObjectRequest()
    }


    override fun onSuccess(requestCode: Int, data: JSONObject?, error: String?) {
        if (1004 == requestCode) {
            handlePopularResponse(data, error)
        }
    }

    private fun handlePopularResponse(data: JSONObject?, error: String?) {
        if (null == error && null != mViewCallBack) {
            val gson = Gson()
            val recentMoviesResponse: RecentAPIResponse = gson.fromJson(data.toString(), RecentAPIResponse::class.java)
            mViewCallBack!!.get()!!.onDataReceived(recentMoviesResponse.data)
        } else if (null != mViewCallBack){
            mViewCallBack!!.get()!!.onError(error!!)
        }


    }


}