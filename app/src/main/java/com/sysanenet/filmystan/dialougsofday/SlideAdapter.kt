package com.sysanenet.filmystan.dialougsofday

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.constraint.ConstraintLayout
import android.support.v4.view.PagerAdapter
import android.support.v7.widget.AppCompatButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.sysanenet.filmystan.R
import com.sysanenet.filmystan.dashboard.DashboardActivity
import com.sysanenet.filmystan.model.MovieDialogModel
import java.util.*

/**
 * Created by ganeshtikone on 20/3/18.
 * Class: SlideAdapter
 */
class SlideAdapter(val context: Context, val movieDialogs: List<MovieDialogModel>) : PagerAdapter() {


    private var textViewDialog: TextView? = null
    private var textViewMovie: TextView? = null
    private var textViewActor: TextView? = null
    private var backgroundView: ConstraintLayout? = null

    private var layoutInflater: LayoutInflater? = null

    private var buttonExplore: AppCompatButton? = null


    val random = Random()

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        val compareObject = `object` as ConstraintLayout
        return (view == compareObject)
    }

    override fun getCount(): Int {
        return movieDialogs.size
    }

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {

        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = layoutInflater?.inflate(R.layout.layout_view_pager, container, false)
        initUI(view, position)
        addListener()
        container?.addView(view)
        return view!!
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(`object` as ConstraintLayout)
    }


    private fun addListener(){
        buttonExplore?.setOnClickListener(View.OnClickListener { view ->
            startDashboard()
        })
    }

    private fun startDashboard() {
        val dashboardIntent = Intent(context,DashboardActivity::class.java)
        context.startActivity(dashboardIntent)
        val  activity = context as DialougesOfDayActivity
        activity.finish()
    }

    /**
     *
     */
    private fun initUI(view: View?, position: Int) {

        backgroundView = view?.findViewById(R.id.slideLayout);
        textViewDialog = view?.findViewById(R.id.textViewDialog)
        textViewMovie = view?.findViewById(R.id.textViewMoviewName)
        textViewActor = view?.findViewById(R.id.textViewActor)
        buttonExplore = view?.findViewById(R.id.buttonExplore)

        backgroundView?.setBackgroundColor(background(position))

        textViewDialog?.text = movieDialogs[position].dialog
        textViewMovie?.text = movieDialogs[position].movieName
        textViewActor?.text = movieDialogs[position].actorName
    }

    fun background(position: Int): Int {

        var backgroundColor: Int = 0

        if (position == 0) {
            backgroundColor = Color.parseColor("#512DA8")
        } else if (position == 1) {
            backgroundColor = Color.parseColor("#009688")
        } else if (position == 2) {
            backgroundColor = Color.parseColor("#212121")
        } else if (position == 3) {
            backgroundColor = Color.parseColor("#303F9F")
        } else if (position == 4) {
            backgroundColor = Color.parseColor("#FF4081")
        } else if (position == 5) {
            backgroundColor = Color.parseColor("#FFA000")
        } else {
            backgroundColor = Color.parseColor("#E64A19")
        }

        return backgroundColor
    }

    fun makeExploreButtonVisible(flag: Boolean){
        if (flag){
            buttonExplore?.visibility = View.VISIBLE
        }else{
            buttonExplore?.visibility = View.GONE
        }
    }
}