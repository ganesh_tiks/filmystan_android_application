package com.sysanenet.filmystan.response

import android.support.annotation.Keep
import com.google.gson.annotations.SerializedName
import com.sysanenet.filmystan.model.MovieModel

/**
 * Created by ganeshtikone on 21/3/18.
 * Class: RecentAPIResponse
 */
@Keep
data class RecentAPIResponse(
        @SerializedName("status") val status: Int,
        @SerializedName("data") val data: List<MovieModel>,
        @SerializedName("msg") val msg: String)