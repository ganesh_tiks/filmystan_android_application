package com.sysanenet.filmystan.model

import android.support.annotation.Keep
import com.google.gson.annotations.SerializedName

/**
 * Created by ganeshtikone on 21/3/18.
 * Data Model Class for Movie Model
 */
@Keep
data class MovieModel(
        @SerializedName("movie_id") val movie_id: String,
        @SerializedName("movie_name") val movie_name: String,
        @SerializedName("movie_poster") val movie_poster: String,
        @SerializedName("movie_dialog_count") val movie_dialog_count: String)