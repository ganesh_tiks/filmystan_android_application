package com.sysanenet.filmystan.dialouges

import com.android.volley.Request
import com.google.gson.Gson
import com.sysanenet.filmystan.network.APIEndPoints
import com.sysanenet.filmystan.network.RESTExecutor
import com.sysanenet.filmystan.network.RESTInterface
import com.sysanenet.filmystan.response.DialogAPIResponse
import org.json.JSONObject
import java.lang.ref.WeakReference

/**
 * Created by ganeshtikone on 22/3/18.
 * Class: DialougesPresenter
 */
class DialougesPresenter(viewCallBack: DialougesMVPContract.ViewCallBackOperations) : DialougesMVPContract.ViewPresenterOperations, RESTInterface {

    private val gson = Gson()

    private var mViewCallBack: WeakReference<DialougesMVPContract.ViewCallBackOperations>? = null

    init {
        mViewCallBack = WeakReference(viewCallBack)
    }

    override fun onDestroy() {
        mViewCallBack = null
    }

    override fun loadRecenetMovies(movieId: String) {
        val apiURL = APIEndPoints.DIALOUGE_DETAIL_DIALOG_URL + movieId
        val restExecutor = RESTExecutor(1002, Request.Method.GET, apiURL)
        restExecutor.setListener(this)
        restExecutor.buildJsonObjectRequest()
    }

    override fun onSuccess(requestCode: Int, data: JSONObject?, error: String?) {

        if (1002 == requestCode) {
            handleDialogDetailResponse(data, error)
        }
    }

    private fun handleDialogDetailResponse(data: JSONObject?, error: String?) {
        if (null == error && null != mViewCallBack) {
            val dialougeMoviesResponse: DialogAPIResponse = gson.fromJson(data.toString(), DialogAPIResponse::class.java)
            mViewCallBack!!.get()!!.onDataReceived(dialougeMoviesResponse.data)
        } else {
            mViewCallBack!!.get()!!.onError(error!!)
        }
    }

}