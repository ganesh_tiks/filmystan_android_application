package com.sysanenet.filmystan.recent

/**
 * Created by ganeshtikone on 22/3/18.
 * Interface: OnMovieTapListener
 */
interface OnMovieTapListener {
    fun onMovieClick(movieId:String, movieName: String)
}