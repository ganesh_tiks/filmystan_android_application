package com.sysanenet.filmystan.dialouges

import com.sysanenet.filmystan.model.DialogueModel

/**
 * Created by ganeshtikone on 24/03/18.
 * Interface: OnDialogListener
 */
interface OnDialogListener {

    fun onShareTapped(dialogueModel: DialogueModel)

    fun onFavouriteTapped(dialogueModel: DialogueModel)

    fun onLongPressed(dialogueModel: DialogueModel)

    fun onWhatsAppTapped(dialogueModel: DialogueModel)
}