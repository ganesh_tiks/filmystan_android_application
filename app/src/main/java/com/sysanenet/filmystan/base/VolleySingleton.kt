package com.sysanenet.filmystan.base

import android.app.Application
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.google.android.gms.ads.MobileAds

/**
 * Created by ganeshtikone on 20/3/18.
 * Class: Volley Singleton Base Class
 */
class VolleySingleton : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        MobileAds.initialize(this, "ca-app-pub-4139698140172535/3690525048")
    }

    val requestQueue: RequestQueue? = null
        get() {
            if (field == null) {
                return Volley.newRequestQueue(this)
            }

            return field
        }

    fun <T> addRequestToQueue(request: Request<T>) {
        request.tag = TAG
        requestQueue?.add(request)
    }

    companion object {
        private val TAG = VolleySingleton::class.java.simpleName
        @get: Synchronized
        var instance: VolleySingleton? = null
            private set
    }
}