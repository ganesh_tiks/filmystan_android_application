package com.sysanenet.filmystan.dialougsofday

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.sysanenet.filmystan.R
import com.sysanenet.filmystan.model.MovieDialogModel
import kotlinx.android.synthetic.main.activity_dialouges_of_day.*

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class DialougesOfDayActivity : AppCompatActivity() {

    var slideAdapter: SlideAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dialouges_of_day)
        setupViewPager()
        addListener()

    }

    /**
     * Add listener
     */
    private fun addListener() {
        viewPager.addOnPageChangeListener(PageChangeListener())
    }

    /**
     * Set up view pager
     */
    private fun setupViewPager() {

        slideAdapter = SlideAdapter(this, createList())
        viewPager.adapter = slideAdapter
        viewPager.setPageTransformer(true, ZoomoutPageTransformer())
    }

    /**
     * Create list from intent
     */
    private fun createList(): List<MovieDialogModel> {

        val movieList = mutableListOf<MovieDialogModel>()

        movieList.add(intent?.extras!!.getParcelable("data")!!)
        movieList.add(intent?.extras!!.getParcelable("data1")!!)
        movieList.add(intent?.extras!!.getParcelable("data2")!!)
        movieList.add(intent?.extras!!.getParcelable("data3")!!)
        movieList.add(intent?.extras!!.getParcelable("data4")!!)

        return movieList
    }


    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private val UI_ANIMATION_DELAY = 300

        private val TAG = DialougesOfDayActivity::class.simpleName
    }

    inner class PageChangeListener:ViewPager.OnPageChangeListener{

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            // not required
        }

        override fun onPageSelected(position: Int) {
            Log.d(TAG, "Selected Page: $position")
            if (position < 4){
                slideAdapter?.makeExploreButtonVisible(false)
            }else{
                slideAdapter?.makeExploreButtonVisible(true)
            }
        }

        override fun onPageScrollStateChanged(state: Int) {
            // not required
        }

    }
}
