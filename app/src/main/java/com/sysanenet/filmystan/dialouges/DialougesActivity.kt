package com.sysanenet.filmystan.dialouges

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.kobakei.ratethisapp.RateThisApp
import com.sysanenet.filmystan.R
import com.sysanenet.filmystan.base.AppDatabaseHelper
import com.sysanenet.filmystan.model.DialogueModel
import com.sysanenet.filmystan.statusmaker.StatusMakerActivity
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_dialouges.*
import kotlinx.android.synthetic.main.content_dialouges.*

class DialougesActivity : AppCompatActivity(), DialougesMVPContract.ViewCallBackOperations, OnDialogListener {


    private var mPresenter: DialougesMVPContract.ViewPresenterOperations? = null
    private var adapter: DialougesAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dialouges)
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        startMVP()
        getMovieMetaData()
        initUI()
        createAdRequest()
        initApplicationRate()
    }

    private fun initApplicationRate() {

        val config = RateThisApp.Config(1, 2)
        RateThisApp.init(config)
        RateThisApp.showRateDialogIfNeeded(this)
    }


    private fun createAdRequest() {

        val adRequest = AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)

                .build()
        adView.loadAd(adRequest)

    }

    private fun initUI() {

        adapter = DialougesAdapter(this, this)

        dialougeRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        dialougeRecyclerView.itemAnimator = DefaultItemAnimator()
        dialougeRecyclerView.adapter = adapter
    }

    private fun startMVP() {
        mPresenter = DialougesPresenter(this)
    }


    override fun onStart() {
        super.onStart()
        shimmerViewContainer.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        shimmerViewContainer.stopShimmerAnimation()
        adView.pause()
    }

    override fun onResume() {
        super.onResume()
        adView.resume()
    }

    override fun onDestroy() {
        super.onDestroy()
        adView.destroy()
    }


    override fun getActivityContext(): Context {
        return this
    }

    override fun getAppContext(): Context {
        return this.applicationContext
    }

    override fun showSnackBarMessage(message: String) {
        // Implement later
    }

    override fun onDataReceived(randomDialog: List<DialogueModel>) {
        shimmerViewContainer.stopShimmerAnimation()
        shimmerViewContainer.visibility = View.GONE
        dialougeRecyclerView.visibility = View.VISIBLE
        adapter!!.updateData(randomDialog)
    }

    override fun onError(error: String) {
        shimmerViewContainer.stopShimmerAnimation()
        shimmerViewContainer.visibility = View.GONE
        showSnackBar("Oops !! Something went wrong please try again !!")
    }

    override fun onShareTapped(dialogueModel: DialogueModel) {
        openShareIntent(dialogueModel)
    }

    override fun onFavouriteTapped(dialogueModel: DialogueModel) {
        addDialougeToFavorite(dialogueModel)
    }

    override fun onWhatsAppTapped(dialogueModel: DialogueModel) {
        val statusMakerIntent = Intent(this, StatusMakerActivity::class.java)
        statusMakerIntent.putExtra("dialogue",dialogueModel.dialog)
        startActivity(statusMakerIntent)
    }


    /**
     * Get movie meta data
     */
    private fun getMovieMetaData() {
        if (null != intent) {

            val name = intent.getStringExtra("name")
            val id = intent.getStringExtra("id")

            supportActionBar!!.title = name
            loadDialougesOfMovie(id)
        }
    }

    /**
     * Load dialouges with movie id
     */
    private fun loadDialougesOfMovie(id: String) {
        mPresenter!!.loadRecenetMovies(id)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item!!.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onLongPressed(dialogueModel: DialogueModel) {

        val dialogue = dialogueModel.dialog
        val movieName = dialogueModel.movie_name.replace("\\s".toRegex(), "")
        val actorName = dialogueModel.actor_name.replace("\\s".toRegex(), "")
        val shareMessage: String = "$dialogue\n Movie: #$movieName\nActor: #$actorName"
        val clipboardService = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("text", shareMessage)
        clipboardService.primaryClip = clipData

        showSnackBar("Dialogue copied to clip board")
    }


    /**
     * Share a dialouge using share intent
     */
    private fun openShareIntent(dialogueModel: DialogueModel) {
        val shareMessage: String = dialogueModel.dialog + "\n" + dialogueModel.movie_name + "\n" + dialogueModel.actor_name + "\nGet Application: https://play.google.com/store/apps/details?id=com.sysanenet.filmystan&hl=en"

        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
        shareIntent.type = "text/plain"
        startActivity(shareIntent)
    }

    /**
     * Add Dialouge to favorite
     */
    private fun addDialougeToFavorite(dialogueModel: DialogueModel) {
        val dbHelper = AppDatabaseHelper(this)
        if (dbHelper.addDialog(dialogueModel)) {
            showSnackBar("Added to Favourite")
        } else {
            showSnackBar("It's already in Favourite !!")
        }
    }

    fun DialougesActivity.showSnackBar(message: String) {

        val snackBar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT)

        snackBar.view.setBackgroundColor(Color.parseColor("#FFC107"))

        snackBar.setActionTextColor(Color.parseColor("#00796B"))

        val textView = snackBar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        textView.setTextColor(Color.WHITE)

        snackBar.show()
    }
}
