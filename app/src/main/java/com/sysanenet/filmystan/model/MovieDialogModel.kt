package com.sysanenet.filmystan.model

import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.Keep

/**
 * Created by ganeshtikone on 20/3/18.
 * Class : Data Model
 */

@Keep
class MovieDialogModel(val dialogId:String, val dialog: String, val tag: String, val id: String, val movieName: String, val actorId: String, val actorName: String) : Parcelable {

    constructor(source: Parcel) : this(source.readString(), source.readString(), source.readString(), source.readString(), source.readString(), source.readString(), source.readString())

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {

        dest?.writeString(dialogId)
        dest?.writeString(dialog)
        dest?.writeString(tag)
        dest?.writeString(id)
        dest?.writeString(movieName)
        dest?.writeString(actorId)
        dest?.writeString(actorName)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<MovieDialogModel> = object : Parcelable.Creator<MovieDialogModel> {
            override fun createFromParcel(source: Parcel): MovieDialogModel {
                return MovieDialogModel(source)
            }

            override fun newArray(size: Int): Array<MovieDialogModel?> {
                return arrayOfNulls(size)
            }
        }
    }
}