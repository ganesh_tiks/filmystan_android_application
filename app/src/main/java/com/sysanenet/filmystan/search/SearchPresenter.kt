package com.sysanenet.filmystan.search

import com.android.volley.Request
import com.google.gson.Gson
import com.sysanenet.filmystan.network.APIEndPoints
import com.sysanenet.filmystan.network.RESTExecutor
import com.sysanenet.filmystan.network.RESTInterface
import com.sysanenet.filmystan.response.RecentAPIResponse
import org.json.JSONObject
import java.lang.ref.WeakReference

/**
 * Created by ganeshtikone on 29/3/18.
 * Class: SearchPresenter
 */
class SearchPresenter(viewCallBack:SearchMVPContract.ViewCallBackOperations): SearchMVPContract.ViewPresenterOperations, RESTInterface {

    var mViewCallBack : WeakReference<SearchMVPContract.ViewCallBackOperations>? = null
    init {
        mViewCallBack = WeakReference(viewCallBack)
    }

    override fun onDestroy() {
        mViewCallBack = null
    }

    override fun onSuccess(requestCode: Int, data: JSONObject?, error: String?) {
        if (1006 == requestCode){
            handleResponse(data,error)
        }
    }

    private fun handleResponse(data: JSONObject?, error: String?) {
        if (null == error && null != mViewCallBack && null != data){
            val gson = Gson()
            val recentMoviesResponse: RecentAPIResponse = gson.fromJson(data.toString(), RecentAPIResponse::class.java)
            mViewCallBack?.get()!!.onDataReceived(recentMoviesResponse.data)
        }else if(null != mViewCallBack){
            mViewCallBack?.get()!!.onError(error!!)
        }
    }

    override fun searchMovie(query: String) {

        var searchQuery = query.replace(" ", "%20")
        val apiURL = "${APIEndPoints.SEARCH_MOVIES_URL}${searchQuery}"
        val restExecutor = RESTExecutor(1006, Request.Method.GET, apiURL)
        restExecutor.setListener(this)
        restExecutor.buildJsonObjectRequest()
    }
}