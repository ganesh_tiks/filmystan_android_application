package com.sysanenet.filmystan.recent

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sysanenet.filmystan.R
import com.sysanenet.filmystan.model.MovieModel
import kotlinx.android.synthetic.main.movie_list_item.view.*

/**
 * Created by ganeshtikone on 21/3/18.
 * Class: RecentMovieAdapter
 */
class RecentMovieAdapter(context: Context, movieTapListener: OnMovieTapListener) : RecyclerView.Adapter<RecentMovieAdapter.RecentMovieViewHolder>() {

    private var context: Context? = null
    private var movieList: MutableList<MovieModel> = mutableListOf<MovieModel>()
    private val listener: OnMovieTapListener = movieTapListener

    init {
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecentMovieViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.movie_list_item, parent, false)
        return RecentMovieViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    override fun onBindViewHolder(holder: RecentMovieViewHolder?, position: Int) {
        holder!!.bindData(movieList[position])
    }


    fun updateData(data: List<MovieModel>) {
        movieList.clear()
        movieList.addAll(data)
        notifyDataSetChanged()
    }

    fun addData(data: List<MovieModel>){
        movieList.addAll(data)
        notifyDataSetChanged()
    }


    inner class RecentMovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindData(movie: MovieModel) {
            val count = "Dialogue Count - " + movie.movie_dialog_count

            itemView.movieName.text = movie.movie_name
            itemView.dialogCount.text = count
            Glide.with(context).load(movie.movie_poster).into(itemView.movieThumbnail)



            itemView.cardViewMovieListItem.setOnClickListener({
                listener.onMovieClick(movie.movie_id, movie.movie_name)
            })
        }
    }
}