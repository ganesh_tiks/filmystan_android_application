package com.sysanenet.filmystan.spalsh

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.animation.AnimationUtils
import com.sysanenet.filmystan.model.MovieDialogModel
import com.sysanenet.filmystan.R
import com.sysanenet.filmystan.dashboard.DashboardActivity
import com.sysanenet.filmystan.dialougsofday.DialougesOfDayActivity
import kotlinx.android.synthetic.main.activity_filmy_stan_splash.*

class FilmyStanSplashActivity : AppCompatActivity(), SplashMVPContract.ViewCallBackOperations {

    /**
     *  Presenter object
     */
    private var mPresenter: SplashMVPContract.ViewPresenterOperations? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filmy_stan_splash)

        startMVP()
        setApplicationVersion()
        //getRandomDialog()
    }

    /**
     * Get Random Dialog from API
     */
    private fun getRandomDialog() {
        mPresenter!!.loadRandomDialog()
    }

    /**
     * Start MVP
     */
    private fun startMVP() {
        mPresenter = FilmyStanSplashPresenter(this)
    }

    override fun onStart() {
        super.onStart()
        animateApplicationName()
    }


    override fun getActivityContext(): Context {
        return this
    }

    override fun getAppContext(): Context {
        return this.applicationContext
    }

    override fun showSnackBarMessage(message: String) {
        Log.d(TAG, message)
    }

    override fun onDataReceived(randomDialog: List<MovieDialogModel>) {
        startRandomDialogActivity(randomDialog)
        Log.e(TAG,randomDialog.toString())
    }

    override fun onError(error: String) {
        Log.e(TAG, error)
    }


    /**
     * Start Random Dialog activity with data model
     */
    private fun startRandomDialogActivity(randomDialog: List<MovieDialogModel>) {
        val dialogOfDayIntent = Intent(this, DialougesOfDayActivity::class.java)
        val bundle = Bundle()
        bundle.putParcelable("data", randomDialog[0])
        bundle.putParcelable("data1", randomDialog[1])
        bundle.putParcelable("data2", randomDialog[2])
        bundle.putParcelable("data3", randomDialog[3])
        bundle.putParcelable("data4", randomDialog[4])
        dialogOfDayIntent.putExtras(bundle)
        startActivity(dialogOfDayIntent)
        finish()
    }


    /**
     * Animate Application Name
     */
    private fun animateApplicationName() {

        val zoomInAnimation = AnimationUtils.loadAnimation(this, R.anim.zoom_in)
        val zoomOutAnimation = AnimationUtils.loadAnimation(this, R.anim.zoom_out)

        val startHandler = Handler()
        startHandler.postDelayed( {
            textViewApplicationName.startAnimation(zoomInAnimation)
        }, 500)

        val stopHandler = Handler()
        stopHandler.postDelayed( {
            textViewApplicationName.startAnimation(zoomOutAnimation)
        }, 1000)

        val navigationHandler = Handler()
        navigationHandler.postDelayed( {
            navigateToDashboard()
        },1800)

    }

    private fun navigateToDashboard() {
        val dashboardIntent = Intent(this,DashboardActivity::class.java)
        startActivity(dashboardIntent)
        finish()
    }

    /**
     * Set Application Current Version
     */
    private fun setApplicationVersion() {
        val buildVersion: String = "version " + getBuildVersion()
        textViewVersion.text = buildVersion
    }

    /**
     * Get application version name
     */
    private fun getBuildVersion(): String {
        return packageManager.getPackageInfo(packageName, 0).versionName
    }

    companion object {
        private val TAG = FilmyStanSplashActivity::class.java.simpleName
    }

}
