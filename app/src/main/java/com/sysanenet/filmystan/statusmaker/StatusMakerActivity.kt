package com.sysanenet.filmystan.statusmaker

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView
import com.sysanenet.filmystan.R
import com.thebluealliance.spectrum.SpectrumDialog
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_status_maker.*
import kotlinx.android.synthetic.main.content_status_maker.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class StatusMakerActivity : AppCompatActivity(), SpectrumDialog.OnColorSelectedListener {

    private var previousSelectedColor: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status_maker)

        fabSave.setOnClickListener { view ->
            saveImage()
        }

        buttonBackgroundPalette.setOnClickListener { view ->
            openPalette()
        }


        setDialogue()

    }

    private fun setDialogue() {

        val dialogue = intent?.getStringExtra("dialogue")
        textViewStatus.text = dialogue
    }

    private fun saveImage() {

        if (runTimePermissionRequired()) {
            if (checkIfExternalStoragePermission()) {
                storeImageOnDevice()
            } else {
                requestExternalStoragePermission()
            }
        } else {
            storeImageOnDevice()
        }

    }

    private fun runTimePermissionRequired(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
    }

    private val requestCode: Int = 2001

    private fun requestExternalStoragePermission() {
        ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                requestCode
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (this.requestCode == requestCode && grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                storeImageOnDevice()
            }
        }
    }

    private fun checkIfExternalStoragePermission(): Boolean {
        val status = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return status == PackageManager.PERMISSION_GRANTED
    }


    private fun storeImageOnDevice() {

        val bitmap = createBitmap()
        WriteFileTask(bitmap).execute()


    }

    private fun createBitmap(): Bitmap {
        val statusBitmap = Bitmap.createBitmap(statusContainer.width, statusContainer.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(statusBitmap)
        statusContainer.layout(statusContainer.left, statusContainer.top, statusContainer.right, statusContainer.bottom)
        statusContainer.draw(canvas)
        return statusBitmap
    }

    override fun onColorSelected(positiveResult: Boolean, color: Int) {
        if (positiveResult) {
            previousSelectedColor = color
            statusContainer.setBackgroundColor(color)
            buttonBackgroundPalette.setBackgroundColor(color)
        }
    }


    /**
     *
     */
    private fun openPalette() {


        val color = R.color.md_teal_500

        SpectrumDialog.Builder(this).setColors(R.array.demo_colors)
                .setSelectedColorRes(color)
                .setDismissOnColorSelected(true)
                .setOutlineWidth(2)
                .setOnColorSelectedListener(this)
                .build()
                .show(supportFragmentManager, "dialog_demo_1")
    }

    fun showSnackBar(error: String) {

        val errorSnackbar = Snackbar.make(statusCoordinator, error, Snackbar.LENGTH_SHORT)

        errorSnackbar.view.setBackgroundColor(Color.parseColor("#FFC107"))

        errorSnackbar.setActionTextColor(Color.parseColor("#00796B"))

        val textView = errorSnackbar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        textView.setTextColor(Color.WHITE)

        errorSnackbar.show()
    }

    inner class WriteFileTask(bitmap: Bitmap) : AsyncTask<Unit, Unit, Boolean>() {

        private var mBitmap: Bitmap = bitmap

        override fun doInBackground(vararg p0: Unit?): Boolean {

            val root = Environment.getExternalStorageDirectory().toString() + File.separator
            val myDir = File(root + getString(R.string.app_name))

            if (!myDir.exists()) {
                if (!myDir.mkdirs()) {
                    return false
                }
            }

            val fileName: String = "" + System.currentTimeMillis() + ".jpg";
            val jpegFile = File(myDir, fileName)

            if (jpegFile.exists()) {
                jpegFile.delete()
            }

            try {
                val fileOutputStream = FileOutputStream(jpegFile)
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream)
                fileOutputStream.flush()
                fileOutputStream.close()

                return true
            } catch (exception: IOException) {
                //Log.e("####", exception.toString())
            }

            return false
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)

            if (result!!) {
                showSnackBar(getString(R.string.image_saved))
                //Log.e("####", "Image saved")
            }
        }

    }

}
