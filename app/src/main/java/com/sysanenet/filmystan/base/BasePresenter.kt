package com.sysanenet.filmystan.base

/**
 * Created by ganeshtikone on 20/3/18.
 * Interface: Base Presenter
 */
interface BasePresenter {

    fun onDestroy()
}