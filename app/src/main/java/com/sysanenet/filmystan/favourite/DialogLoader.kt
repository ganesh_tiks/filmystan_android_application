package com.sysanenet.filmystan.favourite

import android.content.Context
import android.os.AsyncTask
import com.sysanenet.filmystan.base.AppDatabaseHelper
import com.sysanenet.filmystan.model.DialogueModel
import java.lang.ref.WeakReference

/**
 * Created by ganeshtikone on 24/03/18.
 * Class: DialogLoader
 */
class DialogLoader(context: Context, listener: OnFavouriteListener): AsyncTask<Void,Void,List<DialogueModel>>() {

    private var mListener: OnFavouriteListener? = listener
    private var mContext: WeakReference<Context>? = null

    init {
        mContext = WeakReference(context)
    }

    override fun doInBackground(vararg p0: Void?): List<DialogueModel> {

        val dialogList: List<DialogueModel>
        val appDBHelper = AppDatabaseHelper(mContext?.get()!!.applicationContext)

        dialogList = appDBHelper.getAllDialougeList()

        return dialogList
    }

    override fun onPostExecute(result: List<DialogueModel>?) {
        super.onPostExecute(result)

        if (null != mListener){
            mListener!!.onDataLoaded(result!!)
        }
    }
}