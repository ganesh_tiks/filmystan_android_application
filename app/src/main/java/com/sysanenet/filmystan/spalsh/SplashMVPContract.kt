package com.sysanenet.filmystan.spalsh

import com.sysanenet.filmystan.model.MovieDialogModel
import com.sysanenet.filmystan.base.BasePresenter
import com.sysanenet.filmystan.base.BaseViewOpration

/**
 * Created by ganeshtikone on 20/3/18.
 * Interface: MVP Contract
 */
interface SplashMVPContract {

    /**
     * View Call Back interface for Activity
     */
    interface ViewCallBackOperations : BaseViewOpration {

        fun onDataReceived(randomDialog: List<MovieDialogModel>)

        fun onError(error: String)
    }

    /**
     * View Presenter Operation
     */
    interface ViewPresenterOperations : BasePresenter {

        fun loadRandomDialog()
    }
}