package com.sysanenet.filmystan.recent

import com.sysanenet.filmystan.base.BasePresenter
import com.sysanenet.filmystan.base.BaseViewOpration
import com.sysanenet.filmystan.model.MovieModel

/**
 * Created by ganeshtikone on 21/3/18.
 * Interface: RecentMVPContract
 */
interface RecentMVPContract {

    /**
     * View Call Back interface for Activity
     */
    interface ViewCallBackOperations : BaseViewOpration {

        fun onDataReceived(randomDialog: List<MovieModel>)

        fun onError(error: String)
    }

    /**
     * View Presenter Operation
     */
    interface ViewPresenterOperations : BasePresenter {

        fun loadRecenetMovies()
    }
}