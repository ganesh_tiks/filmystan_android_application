package com.sysanenet.filmystan.recent


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.shimmer.ShimmerFrameLayout

import com.sysanenet.filmystan.R
import com.sysanenet.filmystan.dashboard.DashboardActivity
import com.sysanenet.filmystan.dialouges.DialougesActivity
import com.sysanenet.filmystan.model.MovieModel
import kotlinx.android.synthetic.main.fragment_recent.*


/**
 * A simple [Fragment] subclass.
 */
class RecentFragment : Fragment(), RecentMVPContract.ViewCallBackOperations, OnMovieTapListener {


    private var rootView: View? = null
    private var shimmerView: ShimmerFrameLayout? = null

    private var mPresenter: RecentMVPContract.ViewPresenterOperations? = null

    private var adapter: RecentMovieAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startMVP()
    }

    /**
     * Start MVP
     */
    private fun startMVP() {
        mPresenter = RecentFragmentPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.fragment_recent, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initUI()

        adapter = RecentMovieAdapter(activity, this)


        recentMovieRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recentMovieRecyclerView.itemAnimator = DefaultItemAnimator()
        recentMovieRecyclerView.adapter = adapter
        mPresenter!!.loadRecenetMovies()
    }


    override fun getActivityContext(): Context {
        return activity
    }

    override fun getAppContext(): Context {
        return activity.applicationContext
    }

    override fun showSnackBarMessage(message: String) {

        // call parent function
    }

    override fun onDataReceived(randomDialog: List<MovieModel>) {

        shimmerView!!.stopShimmerAnimation()
        shimmerView!!.visibility = View.GONE
        adapter!!.updateData(randomDialog)

    }

    override fun onError(error: String) {
        shimmerView!!.stopShimmerAnimation()
        shimmerView!!.visibility = View.GONE
        (activity as DashboardActivity).showSnackBar("Oops !! Something went wrong please try again !!")
    }

    override fun onMovieClick(movieId: String, movieName: String) {
        val dialougeIntent = Intent(activity, DialougesActivity::class.java)
        dialougeIntent.putExtra("name", movieName)
        dialougeIntent.putExtra("id", movieId)
        startActivity(dialougeIntent)
    }

    /**
     *
     */
    private fun initUI() {
        shimmerView = rootView!!.findViewById(R.id.shimmer_view_container)
    }

    override fun onStart() {
        super.onStart()
        shimmerView!!.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        shimmerView!!.stopShimmerAnimation()
    }

    companion object {
        fun getInstance(): RecentFragment = RecentFragment()
    }

}
