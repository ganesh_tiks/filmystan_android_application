package com.sysanenet.filmystan.network

import org.json.JSONObject

/**
 * Created by ganeshtikone on 20/3/18.
 * Interface : REST Executor Interface
 */
interface RESTInterface {

    fun onSuccess(requestCode:Int, data:JSONObject?, error: String?)
}