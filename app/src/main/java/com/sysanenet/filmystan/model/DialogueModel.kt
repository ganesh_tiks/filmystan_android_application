package com.sysanenet.filmystan.model

import android.support.annotation.Keep
import com.google.gson.annotations.SerializedName

/**
 * Created by ganeshtikone on 22/3/18.
 * Data class: DialogueModel
 */
@Keep
data class DialogueModel(
        @SerializedName("dialog_id") val dialog_id: String,
        @SerializedName("dialog") val dialog: String,
        @SerializedName("tag") val tag: String,
        @SerializedName("movie_id") val movie_id: String,
        @SerializedName("movie_name") val movie_name: String,
        @SerializedName("actor_id") val actor_id: String,
        @SerializedName("actor_name") val actor_name: String)