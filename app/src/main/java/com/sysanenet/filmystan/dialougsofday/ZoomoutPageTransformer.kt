package com.sysanenet.filmystan.dialougsofday

import android.support.v4.view.ViewPager
import android.view.View

/**
 * Created by ganeshtikone on 21/3/18.
 * Class: Zoom Out Page Transformation
 */
class ZoomoutPageTransformer : ViewPager.PageTransformer {


    override fun transformPage(page: View?, position: Float) {

        val pageWidth = page!!.width
        val pageHeight = page.height

        if (position < -1){
            page.alpha = 0.0f
        }else if (position <= 1){

            val scaleVector = Math.max(MIN_SCALE, 1-Math.abs(position))
            val verticalMargin = pageHeight * (1 - scaleVector) / 2
            val horizontalMargin = pageWidth * (1 - scaleVector) / 2

            if (position < 0){
                page.translationX = (horizontalMargin - verticalMargin / 2)
            }else{
                page.translationX = (-horizontalMargin + verticalMargin / 2)
            }

            page.scaleX = scaleVector
            page.scaleY = scaleVector

            page.alpha = ( MIN_ALPHA + (scaleVector - MIN_SCALE) / (1 - MIN_SCALE) * (1 - MIN_ALPHA))

        }else{
            page.alpha = 0.0f
        }
    }


    companion object {
        private val MIN_SCALE = 0.8f
        private val MIN_ALPHA = 0.5f
    }
}