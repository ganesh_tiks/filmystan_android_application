package com.sysanenet.filmystan.recent

import com.android.volley.Request
import com.google.gson.Gson
import com.sysanenet.filmystan.network.APIEndPoints
import com.sysanenet.filmystan.network.RESTExecutor
import com.sysanenet.filmystan.network.RESTInterface
import com.sysanenet.filmystan.response.RecentAPIResponse
import org.json.JSONObject
import java.lang.ref.WeakReference

/**
 * Created by ganeshtikone on 21/3/18.
 * Class: RecentFragmentPresenter
 */
class RecentFragmentPresenter(viewCallBack: RecentMVPContract.ViewCallBackOperations) : RecentMVPContract.ViewPresenterOperations, RESTInterface {


    private var mViewCallBack: WeakReference<RecentMVPContract.ViewCallBackOperations>? = null

    init {
        mViewCallBack = WeakReference(viewCallBack)
    }

    override fun onDestroy() {
        mViewCallBack = null
    }

    override fun loadRecenetMovies() {
        val restExecutor = RESTExecutor(1001, Request.Method.GET, APIEndPoints.RECENT_DIALOG_URL)
        restExecutor.setListener(this)
        restExecutor.buildJsonObjectRequest()
    }

    override fun onSuccess(requestCode: Int, data: JSONObject?, error: String?) {
        if (1001 == requestCode) {
            handleRecentResponse(data, error)
        }
    }

    /**
     * Handle Recent JSON Response
     */
    private fun handleRecentResponse(data: JSONObject?, error: String?) {

        if (null == error && null != mViewCallBack) {
            val gson = Gson()
            val recentMoviesResponse: RecentAPIResponse = gson.fromJson(data.toString(), RecentAPIResponse::class.java)
            mViewCallBack!!.get()!!.onDataReceived(recentMoviesResponse.data)
        } else if (null != mViewCallBack) {
            mViewCallBack!!.get()!!.onError(error!!)
        }
    }
}