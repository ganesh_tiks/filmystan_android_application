package com.sysanenet.filmystan.base

import android.content.Context

/**
 * Created by ganeshtikone on 20/3/18.
 * Interface : Base View Operation
 */
interface BaseViewOpration {

    fun getActivityContext():Context

    fun getAppContext(): Context

    fun showSnackBarMessage(message: String)
}