package com.sysanenet.filmystan.dialouges

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sysanenet.filmystan.R
import com.sysanenet.filmystan.base.AppDatabaseHelper
import com.sysanenet.filmystan.model.DialogueModel
import kotlinx.android.synthetic.main.dialouge_list_item.view.*

/**
 * Created by ganeshtikone on 22/3/18.
 * Class: DialougesAdapter
 */
class DialougesAdapter(context: Context, listener: OnDialogListener) : RecyclerView.Adapter<DialougesAdapter.DialougesViewHolder>() {

    private var context: Context? = context
    private var dialougeList: MutableList<DialogueModel> = mutableListOf<DialogueModel>()
    private var mListener:OnDialogListener = listener


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): DialougesViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.dialouge_list_item, parent, false)
        return DialougesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dialougeList.size
    }

    override fun onBindViewHolder(holder: DialougesViewHolder?, position: Int) {
        holder!!.bindData(dialougeList[position])
    }

    fun updateData(data: List<DialogueModel>) {
        dialougeList.clear()
        dialougeList.addAll(data)
        notifyDataSetChanged()
    }

    inner class DialougesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        val buttonLongClickListener = { v: View ->

            mListener.onLongPressed(dialougeList[adapterPosition])
            true
        }

        fun bindData(dialogueModel: DialogueModel) {
            itemView.textViewDialouge.text = dialogueModel.dialog
            itemView.textViewActor.text = dialogueModel.actor_name

            val tag:String = replaceWebCondeToCharcter(dialogueModel.tag)
            itemView.textViewTag.text = tag


            itemView.imageViewShare.setOnClickListener({ view ->
                mListener.onShareTapped(dialougeList[adapterPosition])
            })

            itemView.imageViewFav.setOnClickListener({ view ->
                mListener.onFavouriteTapped(dialougeList[adapterPosition])
            })


            itemView.imageViewWhatsApp.setOnClickListener({ view ->
                mListener.onWhatsAppTapped(dialougeList[adapterPosition])
            })

            itemView.containerLayout.setOnLongClickListener(buttonLongClickListener)

        }

        private fun replaceWebCondeToCharcter(tag: String): String {
            if (tag.contains("&amp;",false)){
                val modifiedTag = tag.replace("&amp;","&")
                return modifiedTag
            }
            return tag
        }
    }

}