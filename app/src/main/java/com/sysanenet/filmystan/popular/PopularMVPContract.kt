package com.sysanenet.filmystan.popular

import com.sysanenet.filmystan.base.BasePresenter
import com.sysanenet.filmystan.base.BaseViewOpration
import com.sysanenet.filmystan.model.MovieModel

/**
 * Created by ganeshtikone on 22/3/18.
 * Interface: PopularMVPContract
 */
interface PopularMVPContract {

    /**
     * View Call Back interface for Activity
     */
    interface ViewCallBackOperations : BaseViewOpration {

        fun onDataReceived(randomDialog: List<MovieModel>)

        fun onError(error: String)
    }

    /**
     * View Presenter Operation
     */
    interface ViewPresenterOperations : BasePresenter {

        fun loadPopularMovies()
    }
}