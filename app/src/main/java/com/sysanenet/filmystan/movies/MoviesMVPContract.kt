package com.sysanenet.filmystan.movies

import com.sysanenet.filmystan.base.BasePresenter
import com.sysanenet.filmystan.base.BaseViewOpration
import com.sysanenet.filmystan.model.MovieModel

/**
 * Created by ganeshtikone on 23/03/18.
 * Interface: MoviesMVPContract
 */
interface MoviesMVPContract {

    /**
     * View Call Back interface for Activity
     */
    interface ViewCallBackOperations : BaseViewOpration {

        fun onDataReceived(randomDialog: List<MovieModel>)

        fun onError(error: String)
    }

    /**
     * View Presenter Operation
     */
    interface ViewPresenterOperations : BasePresenter {

        fun loadMovies(page:Int)
    }

}