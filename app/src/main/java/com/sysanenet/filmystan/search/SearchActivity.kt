package com.sysanenet.filmystan.search

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.sysanenet.filmystan.R
import com.sysanenet.filmystan.dialouges.DialougesActivity
import com.sysanenet.filmystan.model.MovieModel
import com.sysanenet.filmystan.recent.OnMovieTapListener
import com.sysanenet.filmystan.recent.RecentMovieAdapter
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.content_search.*

class SearchActivity : AppCompatActivity(), SearchMVPContract.ViewCallBackOperations, OnMovieTapListener {


    private var searchQuery: String? = null
    private var adapter: RecentMovieAdapter? = null
    private var mPresenter: SearchMVPContract.ViewPresenterOperations? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        searchQuery = intent.getStringExtra("searchQuery")
        supportActionBar!!.title = searchQuery
        startMVP()
        initUI()
    }

    private fun startMVP() {
        mPresenter = SearchPresenter(this)
    }

    private fun initUI() {

        adapter = RecentMovieAdapter(this, this)

        searchMovieRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        searchMovieRecyclerView.itemAnimator = DefaultItemAnimator()
        searchMovieRecyclerView.adapter = adapter
        mPresenter!!.searchMovie(searchQuery!!)
    }


    override fun onResume() {
        super.onResume()
        shimmerView!!.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        shimmerView!!.stopShimmerAnimation()
    }


    override fun getActivityContext(): Context {
        return this
    }

    override fun getAppContext(): Context {
        return this.applicationContext
    }

    override fun showSnackBarMessage(message: String) {
        // add later
    }

    override fun onDataReceived(randomDialog: List<MovieModel>) {
        shimmerView!!.stopShimmerAnimation()
        shimmerView!!.visibility = View.GONE
        adapter!!.updateData(randomDialog)

    }

    override fun onError(error: String) {
        shimmerView!!.stopShimmerAnimation()
        shimmerView!!.visibility = View.GONE
    }

    override fun onMovieClick(movieId: String, movieName: String) {
        val dialougeIntent = Intent(this, DialougesActivity::class.java)
        dialougeIntent.putExtra("name", movieName)
        dialougeIntent.putExtra("id", movieId)
        startActivity(dialougeIntent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item!!.itemId == android.R.id.home) {
            finish()
            return true
        }


        return super.onOptionsItemSelected(item)
    }
}
