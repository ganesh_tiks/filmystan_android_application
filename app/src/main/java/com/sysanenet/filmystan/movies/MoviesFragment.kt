package com.sysanenet.filmystan.movies


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.shimmer.ShimmerFrameLayout
import com.sysanenet.filmystan.R
import com.sysanenet.filmystan.base.PaginationListener
import com.sysanenet.filmystan.dashboard.DashboardActivity
import com.sysanenet.filmystan.dialouges.DialougesActivity
import com.sysanenet.filmystan.model.MovieModel
import com.sysanenet.filmystan.recent.OnMovieTapListener
import com.sysanenet.filmystan.recent.RecentMovieAdapter
import kotlinx.android.synthetic.main.content_dialouges.*
import kotlinx.android.synthetic.main.fragment_movies.*


/**
 * A simple [Fragment] subclass.
 */
class MoviesFragment : Fragment(), MoviesMVPContract.ViewCallBackOperations, OnMovieTapListener {

    private var rootView: View? = null
    private var shimmerView: ShimmerFrameLayout? = null
    private var adapter: RecentMovieAdapter? = null
    private var mPresenter: MoviesMVPContract.ViewPresenterOperations? = null
    private var layoutManager: LinearLayoutManager? = null


    private var isLoading = false
    private var isLastPage = false

    private var page: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startMVP()
    }

    /**
     * Start MVP
     */
    private fun startMVP() {
        mPresenter = MoviesPresenter(this)
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.fragment_movies, container, false)
        return rootView
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initUI()
        initRecyclerView()
        addScrollListener()
        mPresenter!!.loadMovies(page)

    }

    private fun addScrollListener() {
        movieRecyclerView.addOnScrollListener(PageListener(layoutManager!!))
    }

    private fun initUI() {
        shimmerView = rootView?.findViewById(R.id.shimmer_view_container)
    }

    override fun onStart() {
        super.onStart()
        shimmerView!!.startShimmerAnimation()
    }

    override fun onPause() {
        super.onPause()
        shimmerView!!.stopShimmerAnimation()
    }

    private fun initRecyclerView() {
        adapter = RecentMovieAdapter(activity, this)

        layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        movieRecyclerView.layoutManager = layoutManager
        movieRecyclerView.itemAnimator = DefaultItemAnimator()
        movieRecyclerView.adapter = adapter

    }


    override fun onMovieClick(movieId: String, movieName: String) {
        val dialougeIntent = Intent(activity, DialougesActivity::class.java)
        dialougeIntent.putExtra("name", movieName)
        dialougeIntent.putExtra("id", movieId)
        startActivity(dialougeIntent)
    }

    override fun getActivityContext(): Context {
        return activity
    }

    override fun getAppContext(): Context {
        return activity.applicationContext
    }

    override fun showSnackBarMessage(message: String) {
        // implement later
    }

    override fun onDataReceived(randomDialog: List<MovieModel>) {

        isLoading = false
        shimmerView!!.stopShimmerAnimation()
        shimmerView!!.visibility = View.GONE

        if (randomDialog.isEmpty()) {
            isLastPage = true
        } else {
            adapter!!.addData(randomDialog)
        }


    }

    override fun onError(error: String) {
        shimmerView!!.stopShimmerAnimation()
        shimmerView!!.visibility = View.GONE
        (activity as DashboardActivity).showSnackBar("Oops !! Something went wrong please try again !!")
    }


    private fun loadMoreData() {
        if (!isLoading) {
            page += 1
            mPresenter!!.loadMovies(page)
            isLoading = true
        }
    }


    companion object {
        fun getInstance(): MoviesFragment = MoviesFragment()

        private val TAG = MoviesFragment::class.simpleName
    }


    /**
     * Pagination Listener
     */
    inner class PageListener(layoutManager: LinearLayoutManager) : PaginationListener(layoutManager) {

        override fun loadMoreItems() {
            loadMoreData()
        }

        override fun getTotalPageCount(): Int {
            return 10000
        }

        override fun isLastPage(): Boolean {
            return isLastPage
        }

        override fun isLoading(): Boolean {
            return isLoading
        }

    }
}
