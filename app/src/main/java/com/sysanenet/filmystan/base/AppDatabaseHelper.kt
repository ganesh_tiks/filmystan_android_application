package com.sysanenet.filmystan.base

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.sysanenet.filmystan.model.DialogueModel

/**
 * Created by ganeshtikone on 24/03/18.
 * Class: AppDatabaseHelper
 */
class AppDatabaseHelper(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {


    override fun onCreate(sqliteDatabase: SQLiteDatabase?) {

        val createTableQuery = "CREATE TABLE " + TABLE_NAME + " ( " +
                COL_DIALOUGE_ID + " INTEGER PRIMARY KEY , " +
                COL_DIALOUGE + " TEXT NOT NULL, " +
                COL_MOVIE_NAME + " TEXT NOT NULL, " +
                COL_ACTOR_NAME + " TEXT NOT NULL, " +
                COL_TAG + " TEXT NOT NULL " + ")"

        sqliteDatabase!!.execSQL(createTableQuery)
    }

    override fun onUpgrade(sqliteDatabase: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // change when required
    }

    /**
     * Add data to dialogue model
     */
    fun addDialog(dialouge: DialogueModel): Boolean {

        val contentValue = ContentValues()
        contentValue.put(COL_DIALOUGE_ID, dialouge.dialog_id)
        contentValue.put(COL_DIALOUGE, dialouge.dialog)
        contentValue.put(COL_MOVIE_NAME, dialouge.movie_name)
        contentValue.put(COL_ACTOR_NAME, dialouge.actor_name)
        contentValue.put(COL_TAG, dialouge.tag)

        val sqliteDatabase = writableDatabase
        val lastInsertId = sqliteDatabase.insert(TABLE_NAME, null, contentValue)

        sqliteDatabase.close()

        return lastInsertId > 0
    }

    fun removeDialog(dialouge: DialogueModel): Boolean {

        val sqliteDatabase = writableDatabase
        val numberOfRowsDeleted = sqliteDatabase.delete(
                TABLE_NAME,
                "$COL_DIALOUGE_ID = ? ",
                arrayOf(dialouge.dialog_id)
        )

        sqliteDatabase.close()

        if (numberOfRowsDeleted > 0) {
            return true
        }

        return false
    }

    fun getAllDialougeList(): List<DialogueModel> {

        val dialogList = mutableListOf<DialogueModel>()

        val sqliteDatabase = readableDatabase

        val cursor = sqliteDatabase!!.query(
                TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        )


        if (cursor.moveToFirst()) {

            do {

                dialogList.add(DialogueModel(
                        cursor.getString(cursor.getColumnIndex(COL_DIALOUGE_ID)),
                        cursor.getString(cursor.getColumnIndex(COL_DIALOUGE)),
                        cursor.getString(cursor.getColumnIndex(COL_TAG)),
                        "",
                        cursor.getString(cursor.getColumnIndex(COL_MOVIE_NAME)),
                        cursor.getString(cursor.getColumnIndex(COL_DIALOUGE_ID)),
                        cursor.getString(cursor.getColumnIndex(COL_ACTOR_NAME))
                ))
            } while (cursor.moveToNext())
        }

        cursor.close()
        sqliteDatabase.close()

        return dialogList
    }


    companion object {
        private val DB_VERSION: Int = 1
        private val DB_NAME: String = "Filmystan.sqlite"

        private val TABLE_NAME = "Dialog"
        private val COL_DIALOUGE_ID = "dialogId"
        private val COL_DIALOUGE = "dialog"
        private val COL_MOVIE_NAME = "movie_name"
        private val COL_ACTOR_NAME = "actor_name"
        private val COL_TAG = "tag"

    }
}